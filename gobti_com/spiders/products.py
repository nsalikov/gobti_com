# -*- coding: utf-8 -*-
import json
import js2xml
import scrapy
from urllib.parse import urlparse, urlunparse, parse_qsl, urlencode, quote


class ProductsSpider(scrapy.Spider):
    name = 'products'
    allowed_domains = ['gobti.com']
    start_urls = [
        'http://www.gobti.com/product/finder/AC_ADAPTER',
        'http://www.gobti.com/product/finder/ACCESSORY',
        'http://www.gobti.com/product/finder/CHROMEBOOK_BATT',
        'http://www.gobti.com/product/finder/HANDHELD_SCANNER_BATT',
        'http://www.gobti.com/product/finder/NOTEBOOK_BATT',
        'http://www.gobti.com/product/finder/LAPTOP_BATTERY_CABLE',
        'http://www.gobti.com/product/finder/POWER_TOOL_BATTERY',
        'http://www.gobti.com/product/finder/PROJECTOR_LAMP',
        'http://www.gobti.com/product/finder/UPS_BATT',
    ]

    # http://www.gobti.com/api/product/models?categoryname=AC_ADAPTER&brandname=ACER&page=5&pageSize=25
    # http://www.gobti.com/api/product/models?categoryname=AC_ADAPTER&brandname=FUJITSU%20-%20SIEMENS&page=1&pageSize=25


    def parse(self, response):
        scripts = [s for s in response.css('script[type="text/javascript"] ::text').extract() if 'var categoryKey' in s]

        try:
            script = scripts[0]
            jstree = js2xml.parse(script)
            category_id = jstree.xpath('//var[@name="categoryKey"]/string/text()')[0]
            brands = [b['BrandName'] for b in js2xml.make_dict(jstree.xpath('//var[@name="brandsList"]/array')[0])]
        except Exception as e:
            self.logger.warning('Unable to parse the embedded script: <{}>'.format(response.url), exc_info=True)
            return

        category = ' '.join([s.strip() for s in response.css('h2 ::text').extract()])

        for brand in brands:
            url = 'http://www.gobti.com/api/product/models?categoryname={}&brandname={}&page=1&pageSize=50'.format(category_id, quote(brand))
            meta = {'category_id': category_id, 'category': category, 'brand': brand}
            yield scrapy.Request(url, meta=meta, callback=self.parse_models)


    def parse_models(self, response):
        try:
            models = json.loads(response.text)
        except Exception as e:
            self.logger.warning('Unable to parse json: <{}>'.format(response.url), exc_info=True)
            return

        category_id = response.meta['category_id']
        category = response.meta['category']
        brand = response.meta['brand']

        for model in models:
            url = 'http://www.gobti.com/product/models?categoryname={}&brandname={}&modelno={}'.format(category_id, quote(brand), model['modelNo'])
            meta = {'category_id': category_id, 'category': category, 'brand': brand, 'model': model['modelNo']}
            yield scrapy.Request(url, meta=meta, callback=self.parse_parts)

        if models:
            next_url = get_next_url(response.url)
            meta = {'category_id': category_id, 'category': category, 'brand': brand}
            yield scrapy.Request(next_url, meta=meta, callback=self.parse_models)


    def parse_parts(self, response):
        d = {}

        d['type'] = 'product'
        d['url'] = response.url
        d['breadcrumbs'] = [' '.join([s.strip() for s in item.xpath('.//text()').extract()]) for item in response.css('.breadcrumb .breadcrumb-item')]
        d['category'] = response.meta['category']
        d['brand'] = response.meta['brand']
        d['model'] = response.meta['model']
        d['parts'] = [{'pid': a.css('a::attr(title)').extract_first(), 'url': response.urljoin(a.css('a::attr(href)').extract_first())} for a in response.css('.product-item__title a')]

        yield d

        for part in d['parts']:
            url = 'http://www.gobti.com/product/info?pid={}'.format(quote(part['pid']))
            yield scrapy.Request(url, meta={'pid': part['pid']}, callback=self.parse_part_info)


    def parse_part_info(self, response):
        d = {}

        d['type'] = 'part'
        d['url'] = response.url
        d['breadcrumbs'] = [' '.join([s.strip() for s in item.xpath('.//text()').extract()]) for item in response.css('.breadcrumb .breadcrumb-item')]
        d['pid'] = response.meta['pid']
        d['title'] = ' '.join([s.strip() for s in response.css('.container .border-bottom h2 ::text').extract()])
        d['descripton'] = ' '.join([s.strip() for s in response.css('.container .border-bottom .d-inline-block ::text').extract()])
        d['image'] = response.css('.container img.img-fluid ::attr(src)').extract_first()
        d['specs'] = dict(get_specs(response))

        return d


def get_next_url(request_url):
    next_url = None

    p = list(urlparse(request_url))
    q = dict(parse_qsl(p[4]))

    try:
        q['page'] = int(q['page']) + 1

        p[4] = urlencode(q)
        next_url = urlunparse(p)
    except:
        self.logger.warning('Unable to parse url: <{}>'.format(request_url))

    return next_url


def get_specs(response):
    for li in response.css('.container .mb-2 ul li'):
        text = ' '.join([s.strip() for s in li.css('li ::text').extract()])
        splitted = text.split(':')
        key = splitted[0]
        value = ':'.join(splitted[1:]).strip()

        if not value:
            key = text
            value = 'NA'

        yield (key, value)
